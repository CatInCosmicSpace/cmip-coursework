\section{Сохранение информации}\label{chapter:3}

Сохранение информации и сохранение информации конечного порядка впервые были определены Хаффманом 
в работах \cite{Huffman1, Huffman2}. 

\begin{definition}\label{dfn:3.1}
    Конечный автомат называется сохраняющим информацию, если не существует такого состояния $s_{i}$ 
    и двух разных входных последовательностей $x^{n}$ и $u^{n}$ для $n \geq 1$ таких, что 
    $\lambda(s_{i}, x^{n}) = \lambda(s_{i}, u^{n})$ и $\delta(s_{i}, x^{n}) = \delta(s_{i}, 
    u^{n})$.
\end{definition}

\begin{definition}\label{dfn:3.2}
    Конечный автомат называется сохраняющим информацию конечного порядка $N$, если для каждой пары 
    входных последовательностей длины $N + 1$ с разными символами $x^{N + 1}$ и $u^{N + 1}$ не 
    существует состояния $s_{i}$ такого, что $\lambda(s_{i}, x^{N + 1}) = \lambda(s_{i}, 
    u^{N + 1})$.
\end{definition}

Автоматы, сохраняющие информацию порядка $N$, гарантируют, что знание начального состояния и 
первых $N + 1$ выходных символов позволят восстановить первые входные символы. Автоматы 
\texttt{INV \#}$L$, согласно данному определению, также требуются $L + 1$ выходных символов для 
гарантии результата. Класс автоматов, сохраняющих информацию порядка $N$, также включает классы 
$N - 1$, $N - 2$, $\dots$ и классы нулевого порядка, тогда как \texttt{INV \#}$L$ не является 
\texttt{INV \#}$(L - k), k \geq 1$.

\begin{theorem}\label{th:3.1}
    Конечный автомат $M$ является \texttt{INV \#}$L$ $\Leftrightarrow$ $L$ --- наименьшее такое, 
    что $M$ сохраняет информацию порядка $L$.
\end{theorem}
\begin{proof}
    Если $M$ является \texttt{INV \#}$L$, тогда по теореме~\ref{th:1.3} и определению~\ref{dfn:2.1} 
    не может существовать таких двух входных последовательностей длины $L + 1$, которые отличались 
    бы в первых входных символах и приводили бы к одной и той же выходной последовательности. 
    Таким образом, $M$ удовлетворяет условиям определения~\ref{dfn:3.2} и является сохраняющим 
    информацию порядка $L$.

    С другой стороны, если $M$ является сохраняющим информацию порядка $L$, то выполняется 
    теорема~\ref{th:1.3} для каждого начального состояния и $M$ обратим порядка $L$ в общем случае.
    Так как $L$ наименьшее такое, что $M$ существует общий обратный автомат с задержкой $L$, 
    отсюда следует, что $M$ является \texttt{INV \#}$L$.
\end{proof}

Как указано Хаффманом, более общий класс автоматов с сохранением информации не обратим. Интересно 
заметить, что в случае минимального автомата с сохранением информации, возможно составить такой 
эксперимент, который бы смог определить для любого $n$ входную последовательность длины $n$ из 
знания начального состояния и последних $n$ выходных символов, а также по конечному состоянию. 
Как показано в \cite{Gill}, такие эксперименты всегда существуют в случае минимального автомата. 
Эксперименты по определению конечного состояния состоят из применения к автомату входной 
последовательности, которая приводит к известному конечному состоянию вне зависимости от состояний 
$S_{f}(s_{i}, \alpha^{n})$, принимаемые автоматом во время наблюдения выходной последовательности 
$\alpha^{n}$ из начального состояния $s_{i}$. Актуальность термина ``сохранение информации'' 
очевидна. Входная последовательность всегда может быть восстановлена6. В некоторых случаях возможна 
другая интересная характеристика автоматов с сохранением информации, которую приведём в 
теореме~\ref{th:3.3}.

Для начала рассмотрим свойства автоматов с сохранением информации и автоматов с сохранением 
информации конечного порядка. Следующая теорема следует из \cite{Even1}.

\begin{theorem}\label{th:3.3}
    Если автомат является автоматом с сохранением информации конечного порядка (\texttt{ILF}) 
    (а следовательно и обратимым), он также является автоматом с сохранением информации 
    (\texttt{IL}).
\end{theorem}

Обратное утверждение не будет верным. Это можно увидеть на примере счётчика. 
Автомат на рисунке~\ref{fig:3.1} пример такого счётчика.

\begin{figure}[ht!]
    \centering
    \begin{tabular}{l|c|c|}
        \diagbox{$S$}{$X$} & $0$ & $1$\\ \hline
        $s_{1}$ & $s_{2} / 1$ & $s_{3} / 1$ \\ \hline
        $s_{2}$ & $s_{1} / 1$ & $s_{3} / 0$ \\ \hline
        $s_{3}$ & $s_{2} / 0$ & $s_{1} / 0$ \\ \hline
    \end{tabular}
    \caption{Бинарный автомат \texttt{IL}, но не \texttt{ILF}}
    \label{fig:3.1}
\end{figure}

Факт того, что автомат на рисунке~\ref{fig:3.1} является \texttt{IL}, но не \texttt{ILF} легко 
определяется проверкой, описанной далее. Некоторые процедуры тестирования описаны в работах 
\cite{Even1, Huffman2, Hennie}. Многие эффективные тесты предложены Эвеном и включают в себя одну 
простую процедуру проверки как условий для \texttt{IL}, так и для \texttt{ILF}. Она может быть 
описана так:
\begin{enumerate}
    \item[1.] Рассмотрим возможные пути на диаграмме переходов состояний, которые приводят в 
    заданное конечное состояние с известными выходными метками. Построим ``граф тестирования 
    обратимости'' (ГТО), взяв пары путей и отмечая последовательные узлы на ГТО парами состояний 
    на одном уровне от рассматриваемого состояния.  Пары состояний, маркирующие каждую вершину 
    графа, называются совместимыми парами. Точнее, дадим определение.
        \begin{definition}\label{dfn:3.3}
            Два состояния $s_{i}$ и $s_{j}$ называются совместимой парой $(s_{i}, s_{j})$, если:
            \begin{enumerate}
                \item[I.] Существует такое состояние $s_{k}$ и входные символы $x$ и $u$ такие, 
                что $\delta(s_{k}, x) = s_{i}, \delta(s_{k}, u) = s_{j}$ и 
                $\lambda(s_{k}, x) = \lambda(s_{k}, u)$; или
                \item[II.] Существует совместимая пара $(s_{n}, s_{m})$ и входная
                 последовательность $x$ и $u$ такие, что $\delta(s_{n}, x) = s_{i}, 
                 \delta(s_{m}, u) = s_{j}$ и $\lambda(s_{n}, x) = \lambda(s_{m}, u)$.
            \end{enumerate}
        \end{definition}

    \item[2.] $M$ является \texttt{IL} тогда и только тогда, когда ГТО для $M$ не содержит 
    совместимых пар вида $(s_{i}, s_{i})$ для любого $s_{i} \in S$.
    \item[3.] $M$ является \texttt{ILF} порядка $N$ тогда и только тогда, когда ГТО для $M$ не 
    содержит циклов и наибольшая цепь совместимых пар (вершин) ГТО имеет не более $N$ вершин. 
\end{enumerate}

Для иллюстрации данной процедуры, рассмотрим автомат с рисунка~\ref{fig:3.1} и приведём ГТО на 
рисунке~\ref{fig:3.2}.

\begin{figure}[ht!]
    \centering
    \begin{tikzpicture}[>=stealth,auto,node distance=3cm]
        \node[state] (s2s3)                       {$(s_{2}, s_{3})$};
        \node[state] (s1s3) [below right of=s2s3]    {$(s_{1}, s_{3})$};
        \node[state] (s1s2) [below left of=s1s3]   {$(s_{1}, s_{2})$};

        \path[->] (s2s3) edge[loop above] node {~} (s2s3);
        \path[->] (s1s2) edge[loop below] node {~} (s1s2);
        \path[->] (s2s3) edge node {~} (s1s3);
        \path[->] (s1s2) edge node {~} (s1s3);
    \end{tikzpicture}
    \caption{Тест обратимости автомата с рисунка~\ref{fig:3.1}}
    \label{fig:3.2}
\end{figure}%

Полный ГТО показан на рисунке~\ref{fig:3.2}, он не имеет пар вида $(s_{i}, s_{i})$. Следовательно, 
конечный автомат с рисунка~\ref{fig:3.1} является \texttt{IL}. В ГТО присутствуют циклы в вершинах 
$(s_{2}, s_{3})$ и $(s_{1}, s_{2})$ (являющиеся петлями), поэтому данный конечный автомат не 
является \texttt{ILF} и, следовательно, не обратим.

Количество совместимых пар состояний, которые появляются в ГТО для любого конечного автомата $M$, 
может быть найдено путем рассмотрения всех отдельных пар состояний в множестве конечных состояний 
$M$. Этот вывод вытекает из следующей леммы.

\begin{lemma}\label{lem:3.1}
    Для любого конечного автомата $M$, если $s_{i}$ и $s_{j} \in S_{f}(s_{k}, \alpha^{n})$ для 
    некоторых $s_{k}$ и $\alpha^{n}$ и $(s_{i}, s_{j})$ является совместимой парой в ГТО для $M$. 
    Наоборот, если $(s_{i}, s_{j})$ является совместимой парой на ГТО для $M$, то существуют такие 
    $s_{k}$ и $\alpha^{n}$ для некоторого $n$ такие, что $s_{i}$ и $s_{j} \in S_{f}(s_{k}, 
    \alpha^{n})$.
\end{lemma}
\begin{proof}
    Доказательство следует напрямую из определений~\ref{dfn:1.6} и \ref{dfn:3.3}.
\end{proof}

Теперь обратимся к дополнительной характеристике определённого класса автоматов с сохранением 
информации, о которой упоминали ранее. В качестве ограничения здесь выступает тот же класс конечных 
автоматов, рассмотренных в части \ref{chapter:2}, а именно --- сильно связные с $\#X = \#Y$.

\begin{theorem}\label{th:3.2}
    Если $M$ сильно связный автомат с $\#X = \#Y$, то $M$ является \texttt{IL} $\Leftrightarrow$ 
    $\displaystyle \bigcup_{s \in S} Y^{n}(s) = Y^{n} \forall n \geq 0$.
\end{theorem}
\begin{proof}
    \noindent\fbox{$\Leftarrow$} Сначала докажем достаточность. Покажем, если $M$ не является 
    \texttt{IL}, то существует некоторая последовательность $y^{n}$ для некоторого $n \geq 1$ 
    такая, что $y^{n}$ не выводится ни одним состоянием. Для этого используем следующую лемму.

    \begin{lemma}\label{lem:3.2}
        Если сильно связный конечный автомат не \texttt{IL}, то существует целое $n \geq 1$ такое, 
        что для каждого $s \in S$ и всех $m \geq n$ существует $x^{m} \neq u^{m}$ такие, что 
        $\delta(s, x^{m}) = \delta(s, u^{m})$ и $\lambda(s, x^{m}) =\lambda(s, u^{m})$.
    \end{lemma}
    \begin{proof}
        Предположение, что $M$ не \texttt{IL} предполагает существование $s_{0}$ и $s_{f} \in S$ и 
        $a^{i} \neq b^{i} \in X^{i}$ для некоторого $i$ таких, что $\delta(s_{0}, a^{i}) = 
        \delta(s_{0}, b^{i}) = s_{f}$ и $\lambda(s_{0}, a^{i}) = \lambda(s_{0}, b^{i})$. 
        Из свойства сильной связности следует, что существует $v^{j}$ для некоторого 
        $j \leq N - 1$ (где $N = \#S$) для всех $s \in S$ такая, что $\delta(s, v^{j}) = s_{0}$. 
        Пусть $x^{n} = v^{j}a^{i}$ и $u^{n} = v^{j}b^{i}$. Очевидно $n \leq N - 1 + i$ и 
        $\delta(s, x^{n}) = \delta(s, u^{n})$ и $\lambda(s, x^{n}) = \lambda(s, u^{n})$. Так как 
        конечное состояние одинаково для $x^{n}$ и $u^{n}$, то $x^{m} = x^{n}w^{k}$ и $u^{m} = 
        u^{n}w^{k}$, где $w^{k}$ любая входная последовательность, удовлетворяющая требованиям 
        леммы.
    \end{proof}
    Рассмотрим выходную последовательность длины $k$ --- конечные пары состояний для состояния 
    $s$ можно определить согласно~\ref{dfn:1.7}: $F^{k}(s) = \left\{(y^{k}, s_{k}): \exists 
    x^{k}: \delta(s, x^{k}) = s^{k} \wedge \lambda(s, x^{k}) = y^{k}\right\}$.

    Из леммы~\ref{lem:3.2} следует, что для любого $s \in S$ существует некоторое $n$ такое, что 
    для всех $m \geq n$ $\#F^{m}(s) \leq q^{m} - 1$, где $q = \#X = \#Y$. Это следует из того, 
    что каждая пара $(y^{m}, s_{m})$ из $F^{m}(s)$ должна из разных входных последовательностей 
    длины $m$ и хотя бы двух возможных из $q^{m}$ возможных входных последовательностей приводить 
    к одной и той же паре.

    Это может быть проверено для любого $k, j \geq 1$ и для любого $s \in S$. $$F^{k + j}(s) = 
    \left\{(y^{k}y^{j}, s_{f}): \exists s_{k}: (y^{k}, s_{k}) \in F^{k}(s) \wedge (y^{i}, s_{f}) 
    \in F^{j}(s_{k})\right\}$$
    
    Используя такое определение $F^{k + j}(s)$ возможно показать следующую лемму.

    \begin{lemma}\label{lem:3.3}
        $\displaystyle \#F^{k + j}(s) \leq \#F^{k}(s) \cdot \max_{k}\#F^{j}(s_{k})$.
    \end{lemma}
    \begin{proof}
        Доказательство следует из того факта, что $$\#F^{k + j}(s) \leq \#\left\{\left[(y^{k}, 
        s_{k}), (y^{j}, s_{f})\right]: (y^{k}, s_{k}) \in F^{k}(s) \wedge (y^{j}, s_{f}) \in 
        F^{j}(s_{k})\right\}.$$
        Неравенство возможно в вышеприведенном выражении, так как более чем одно состояние $s_{k}$ 
        может быть связано с фиксированным выбором $y_{k}$, и $s_{f}$. Очевидно, что если мы 
        выберем $s_{k}$ таким образом, что $\#F_{j}(s_{k})$ будет максимальным, то $\#F^{k+j}(s) 
        \leq \#F^{k} (s) \cdot \#F^{j}(s_{k})$ и лемма будет доказана.
    \end{proof}

    Применяя результат леммы~\ref{lem:3.3} для $j = m$ и учитывая, что $\#F^{m}(s_{k}) 
    \leq q^{m} - 1$, то для автомата $M$ $\#F^{k + m}(s) \leq \#F^{k}(s) \cdot (q^{m} - 1)$ для 
    всех $k, s \in S$. Из этого следует, что для любого $t \geq 1$ $\#F^{tm}(s) \leq 
    (q^{m} - 1)^{t}$, что приводит к $\displaystyle \sum_{s \in S} \#F^{tm}(s) \leq N \cdot 
    (q^{m} - 1)^{t}$.

    С другой стороны, предположим, что для любой выбранной $y^{n}$ для любого $n$ существует 
    некоторое состояние из $S$, которое приводит к $y^{n}$. Это эквивалентно $\displaystyle 
    \bigcup_{s \in S} Y^{n}(s) = Y^{n}$. Для $n = tm$ тогда $\displaystyle \bigcup_{s \in S} 
    Y^{tm}(s) = Y^{tm}$. Поэтому $\displaystyle \sum_{s \in S} \#Y^{tm}(s) \geq q^{tm}$. Но 
    также справедливо, что $\#Y^{n}(s) \leq \#F^{n}(s)$. Таким образом, $\displaystyle \sum_{s 
    \in S}\#F^{tm}(s) \geq q^{tm}$. 

    Объединив верхние и нижние границы для $\displaystyle \sum_{s \in S}\#F^{tm}(s)$, заметим, 
    что $q^{tm} \leq N \cdot (q^{m} - 1)^{t}$, а значит и $N \geq (q^{m} / (q^{m} - 1))^{t} 
    \quad\forall t$. 

    Очевидно, что результат невозможен, так как правая сторона с ростом $t$ не ограничена и будет в 
    конечном итоге превышать $N$. Поэтому делаем вывод, если $M$ не \texttt{IL}, то существует 
    некоторое $n$ и выходная последовательность $y^{n}$ такие, что $y^{n}$ не может быть выведена 
    автоматом. Следовательно, достаточность доказана. 

    \noindent \fbox{$\Rightarrow$} Предположим, что существует $n$ для некоторой $\alpha^{n} \in 
    Y^{n}$ такой, что $\alpha^{n}$ не может быть выведена автоматом и покажем, что $M$ не 
    \texttt{IL}. Пусть $M$ является \texttt{IL}. Из определения~\ref{dfn:3.1} следует, что 
    $(y^{kn}, s_{f}) \in F^{kn}(s)$ подразумевает входную последовательность $x^{kn}~\forall s 
    \in S$. Поэтому должно выполняться неравенство $\#F^{kn}(s) \geq q^{kn}$. Но если $\alpha^{n}$ 
    не может быть выведена автоматом, то $\#Y^{n}(s) \leq q^{n} - 1$, а далее $\#Y^{kn}(s) \leq 
    (q^{n} - 1)^{k}~\forall k \geq 1$. Кроме того, для $N = \#S$ выполняется $\#F^{kn}(s) \leq N 
    \cdot (q^{n} - 1)^{k}$. Объединяя верхнюю и нижнюю границы для $\#F^{kn}(s)$, получаем $N 
    \cdot (q^{n} - 1)^{k} \geq q^{nk}$, откуда следует, что $N \geq (q^{n} / 
    (q^{n} - 1))^{k}~\forall k$. Результат аналогичен доказательству достаточности. Поэтому 
    предположение, что $\alpha^{n}$ не может быть выведено автоматом означает, что $M$ не 
    является \texttt{IL}. Необходимость доказана.
\end{proof}

Условия теоремы~\ref{th:3.3} для свойства \texttt{IL} могут быть сравнены с похожими результатами 
теоремы~\ref{th:2.3} для обратимости \texttt{INV \#}$L$. Одинаковый класс конечных автоматов 
рассматривается в обоих случаях --- сильно связные автоматы с множеством входных и выходных 
символов одного порядка. Как обратимые автоматы, так и автоматы с сохранением информации 
подразумевают, что можно произвольно выбрать выходную последовательность любой длины и найти 
состояние, которое привело к такой последовательности. Обратимость, будучи более сильным свойством, 
требует, чтобы такое состояние было достижимо за $L$ шагов, где $L$ --- задержка обратимости. 
Поэтому для \texttt{INV \#}$L$ автоматов любой ответ может быть получен после переходного процесса 
длины $L$.

Рассматривая свойство \texttt{IL} для общего класса автоматов приводит к вопросам, касающихся 
границ. В частности, мы можем задаться вопросом, какое максимальное целое $n$ такое, что не 
существует такого состояния $s$ или входной последовательности $x^{n} \neq u^{n}$, удовлетворяющих 
$\delta(s, x^{n}) = \delta(s, u^{n})$ и $\lambda(s, x^{n}) = \lambda(s, u^{n})$ у не-\texttt{IL} 
автомата. Мы можем вывести верхнюю границу этого числа для общего класса конечных автоматов.

\begin{theorem}\label{th:3.4}
    Если конечный автомат $M$ не \texttt{IL}, то существуют $s_{0}$ и $a^{n} \neq b^{n}$ такие, 
    что $\lambda(s_{0}, a^{n}) = \lambda(s_{0}, b^{n})$ и $\delta(s_{0}, a^{n}) = \delta(s_{0}, 
    b^{n})$ для некоторого $n \leq C_{N}^{2} + 1, N = \#S$.
\end{theorem}
\begin{proof}
    Пусть $M$ любой не-\texttt{IL} автомат. Предположим, что наименьшее $n$ такое, что существует 
    $s_{0}, a^{n}$ и $b^{n}$ удовлетворяют требованиям теоремы, большее, чем $C_{N}^{2} + 1$ $(n > 
    C_{N}^{2} + 1)$. Пусть $a^{n} = a_{1}a_{2}\dots a_{n}$ и $b^{n} = b_{1}b_{2}\dots b_{n}$. 
    Так как может быть только $C_{N}^{2}$ совместимых пар, для некоторых $i, j: i < j 
    \leq C_{N}^{2}$ следует, что $\delta(s_{0}, a_{1}a_{2}\dots a_{i}) = \delta(s_{0}, 
    a_{1}a_{2}\dots a_{j})$ и $\delta(s_{0}, b_{1}b_{2}\dots b_{i}) = \delta(s_{0}, 
    b_{1}b_{2}\dots b_{j})$ или $\delta(s_{0}, a_{1}a_{2}\dots a_{i}) = \delta(s_{0}, 
    b_{1}b_{2}\dots b_{j})$ и $\delta(s_{0}, b_{1}b_{2}\dots b_{i}) = \delta(s_{0}, 
    a_{1}a_{2}\dots a_{j})$.

    Так как $\delta(s_{0}, a^{n}) = \delta(s_{0}, b^{n})$ и $\lambda(s_{0}, a^{n}) = 
    \lambda(s_{0}, b^{n})$, существует входная последовательность длины $n - j$, например, 
    $c^{n - j}$ и $d^{n - j}$ такие, что $\delta(s_{0}, a_{1}a_{2}\dots a_{i}c^{n - j}) = 
    \delta(s_{0}, b_{1}b_{2}\dots b_{i}d^{n - j})$ и $\lambda(s_{0}, 
    a_{1}a_{2}\dots a_{i}c^{n - j}) = \lambda(s_{0}, b_{1}b_{2}\dots b_{i}d^{n - j})$.

    Но $i < j$, поэтому $i + n - j < n$ и $n$ не является наименьшим целым таким, что выполняются 
    условия теоремы. Соответственно, минимальное число должно быть меньше, чем $C_{N}^{2} + 1$.
\end{proof}

Теорема~\ref{th:3.3} подразумевает, что существует некое максимальное целое число $n$ такое, что 
для любой $y^{n}$ может быть найдены $s$ и $y^{n}$, удовлетворяющих $\lambda(s, x^{n}) = y^{n}$ 
для сильно связного не-\texttt{IL} автомата с $N$ состояниями и $\#X = \#Y = q$. Мы не можем 
связать это число с $n$. Получена очень неточная верхняя граница для $n$, которая следует из 
леммы~\ref{lem:3.2} и теоремы~\ref{th:3.4} и дана в терминах $N$ и $q$. Граница быстро растет 
вместе с $N$ и не представлена в этой работе. Мы отмечаем, что в случае двоичных автоматов 
привязка к $n$ осуществляется соотношением $n \leq 2(N - 1)$. Однако мы не смогли доказать, 
что это предположение связано с бинарным случаем.

В этой части мы представим еще один результат, который тесно связан с работой Хенни \cite{Hennie}. 
Хенни определяет тест для свойства \texttt{ILF}, который, с точки зрения наших параметров, состоит в 
основном из теста на порядок пересечения наборов начальных и конечных состояний. Небольшое 
расширение и упрощение процедуры Хенни представлено в следующем результате.

\begin{theorem}\label{th:3.5}
    Конечный автомат \texttt{IL} $M$ является \texttt{INV \#}$L$ $\Leftrightarrow$ $L$ --- 
    наименьшее целое такое, что $\#\left\{S_{f}(s_{i}, \alpha) \cap S_{i}(\beta^{L})\right\} < 
    2~\forall s_{i} \in S, \alpha \in Y(s_{i})$ и $\beta^{l} \in Y^{L}$.
\end{theorem}
\begin{proof}
    \noindent \fbox{$\Leftarrow$} Предположим, что $\#\left\{S_{f}(s_{i}, \alpha) \cap 
    S_{i}(\beta^{L})\right\} < 2\quad \forall s_{i} \in S, \alpha \in Y^{s_{i}}$ и $\beta^{L} 
    \in Y^{L}$ для некоторого целого $L$. Существует два варианта --- либо $\#\left\{S_{f}(s_{i}, 
    \alpha) \cap S_{i}(\beta^{L})\right\} = 0$ или $\#\left\{S_{f}(s_{i}, \alpha) \cap S_{i}
    (\beta^{L})\right\} = 1$.

    Так как $M$ является \texttt{IL}, то по определению~\ref{dfn:3.1} не существует двух входных 
    символов $x \neq u$ для некоторого $s_{i} \in S$ такие, что $\lambda(s_{i}, x) = 
    \lambda(s_{i}, u)$ и $\delta(s_{i}, x) = \delta(s_{i}, u)$. Таким образом, для некоторого 
    $s_{i}$ существует как минимум один входной символ $x$ такой, что $\lambda(s_{i}, x) = 
    \alpha, \delta(s_{i}, x) \in S_{i}(\beta^{L})$ для некоторых $\alpha \in Y(s_{i})$ и 
    $\beta^{L} \in Y^{L}$. По определению~\ref{dfn:3.2} $M$ является \texttt{ILF} порядка $L$ и по 
    теореме~\ref{th:3.1} $M$ также \texttt{INV \#}$L$.

    \noindent \fbox{$\Rightarrow$} Предположим, что не существует $L$ такого, что 
    $\#\left\{S_{f}(s_{i}, \alpha) \cap S_{i}(\beta^{L})\right\} < 2\quad \forall s_{i}, 
    \alpha$ и $\beta^{L}$. Отсюда следует, что для любого $L$ существует некоторое $s_{i}$ и 
    два входных символа $x \neq u$ такие, что $\lambda(s_{i}, x) = \lambda(s_{i}, u) = \alpha, 
    \delta(s_{i}, x) \in S_{i}(\beta^{L})$ и $\delta(s_{i}, u) \in S_{i}(\beta^{L})$ для 
    некоторых $\alpha \in Y, \beta^{L} \in Y^{L}$. Очевидно, что $\lambda(s_{i}, xa^{L}) = 
    \lambda(s_{i}, ub^{L}) = \alpha\beta^{L}$ для некоторых $a^{L}, b^{L} \in X^{L}$. По 
    определению~\ref{dfn:3.2} $M$ не \texttt{ILF} порядка $L$ и по теореме~\ref{th:3.1} не 
    \texttt{INV \#}$L$.
 \end{proof}

 Применение результата теоремы~\ref{th:3.5} для проверки свойств \texttt{ILF}, а следовательно, и 
 обратимости, отличается от теста Хенни тем, что в нашем тесте необходимо рассматривать наборы 
 конечных состояний $S_{f}(s_{i}, \alpha)$ для последовательностей $\alpha$ длины 1. Формулировка 
 таблицы прямого теста Хенни требует, чтобы таблица была расширена и включала все возможные 
 отдельные множества $S_{f}(s_{i}, \alpha^{n})$ для всех $s_{i}, \alpha^{n}, n = 1, 2, 3, \dots$. 
 В заключение отметим, что прежде чем результат теоремы~\ref{th:3.5} может быть применен для 
 исследования свойств \texttt{ILF}, необходимо сначала проверить автомат на \texttt{IL}. 
 Существуют автоматы, для которых $\#\left\{S_{f}(s, \alpha) \cap S_{i}(\beta^{L})\right\} < 
 2~\forall s \in S, \alpha \in Y(s)$ и $\beta^{L} \in Y^{L}$, но не \texttt{INV \#}$L$, если 
 условия для \texttt{IL} не выполняются. Автомат на рисунке~\ref{fig:3.3} является примером для 
 $L = 1$.

 \begin{figure}[ht!]
    \centering
    \begin{tabular}{l|c|c|}
        \diagbox{$S$}{$X$} & $0$ & $1$\\ \hline
        $s_{1}$ & $s_{2} / 1$ & $s_{2} / 1$ \\ \hline
        $s_{2}$ & $s_{1} / 1$ & $s_{3} / 1$ \\ \hline
        $s_{3}$ & $s_{2} / 0$ & $s_{3} / 0$ \\ \hline
    \end{tabular}
    \caption{Не-\texttt{IL} конечный автомат}
    \label{fig:3.3}
\end{figure}

В данной части мы рассмотрели понятия сохранения информации и сохранения информации конечного 
порядка. Эти понятия были связаны с нашими определениями обратимости. Было представлено необходимое 
и достаточное условие для того, чтобы определенный класс конечных автоматов был \texttt{IL}, и были 
рассмотрены некоторые границы, связанные со свойством \texttt{IL}. В следующей главе мы рассмотрим 
применение процедуры восстановления определенного состояния и ее влияние на обратимость конечных 
автоматов.
